<?php

use Illuminate\Database\Seeder;
use App\DetalleLibro;

class DetalleLibroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 50; $i++) {
            factory(DetalleLibro::class)->create();
        }
    }
}
