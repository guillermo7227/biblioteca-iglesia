<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DetalleLibro;
use Faker\Generator as Faker;
use App\Autor;
use App\Libro;

$factory->define(DetalleLibro::class, function (Faker $faker) {
    $datos = registroNuevo($faker);
    while (!$datos) {
        $datos = registroNuevo($faker);
    }
    return [
        'libro_id' => $datos['libroId'],
        'autor_id' => $datos['autorId'],
    ];
});

function registroNuevo(Faker $faker)
{
    $tempLibroId = $faker->randomElement(Libro::all()->pluck('id')->all());
    $tempAutorId = $faker->randomElement(Autor::all()->pluck('id')->all());

    $registroExiste = DetalleLibro::where([
        ['autor_id', $tempAutorId],
        ['libro_id', $tempLibroId],
    ])->exists();

    if ($registroExiste) return false;

    return ['libroId' => $tempLibroId, 'autorId' => $tempAutorId];
}
