<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Prestamo;
use Faker\Generator as Faker;
use App\Persona;
use App\Libro;

$factory->define(Prestamo::class, function (Faker $faker) {
    $fechaInicio = now()->subDays($faker->numberBetween(1, 20))->format('Y-m-d');
    $fechaEntrega = now()->addDays($faker->numberBetween(1, 20))->format('Y-m-d');
    return [
        'libro_id' => $faker->randomElement(Libro::all()->pluck('id')->all()),
        'persona_id' => $faker->randomElement(Persona::all()->pluck('id')->all()),
        'fecha_inicio' => $fechaInicio,
        'fecha_entrega' => $fechaEntrega,
        'entregado' => false,
    ];
});
