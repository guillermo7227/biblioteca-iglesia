<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPrestamos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('libro_id')->unsigned();
            $table->bigInteger('persona_id')->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_entrega');
            $table->boolean('entregado')->default(false);
            $table->timestamps();

            $table->foreign('libro_id')->references('id')->on('libros');
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestamos');
    }
}
