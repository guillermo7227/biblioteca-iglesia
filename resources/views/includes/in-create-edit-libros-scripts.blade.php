  <script src="{{ asset('js/quill.min.js') }}"></script>
  <script>
    var quill = new Quill('.editor', {
      modules: {
        toolbar: [
          [{ header: [1, 2, false] }],
          ['bold', 'italic', 'underline', { 'align': [] }, 'code-block'],
          ['blockquote', { 'list': 'ordered'}, { 'list': 'bullet' },
            { 'indent': '-1'}, { 'indent': '+1' }, 'link'],
        ]
      },
      placeholder: 'Escriba una descripción...',
      theme: 'snow'  // or 'bubble'
    });

    quill.on('text-change', function(delta, oldDelta, source) {
      $('[name=descripcion]').val(quill.root.innerHTML)
    });
  </script>

