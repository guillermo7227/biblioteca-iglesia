
@if(config('app.env') == 'production')
  <script src="{{ asset('js/vue.min.js') }}"></script>
@else
  <script src="{{ asset('js/vue.js') }}"></script>
@endif
