
<div class="d-flex justify-content-center mt-3">
  @if($libro->estaDisponible)
    <h5 class="text-success text-center">
      <i class="fa fa-check"></i>
      Disponible para préstamo
    </h5>
  @else
    @if($libro->hayEnInventario)
      <h5 class="text-warning text-center">
        <i class="fa fa-hourglass-half"></i>
        No disponible en el momento
      </h5>
    @else
      <h5 class="text-danger text-center">
        <i class="fa fa-times"></i>
        No hay en inventario
      </h5>
    @endif

  @endif
</div>

