
      <div class="d-flex justify-content-center mt-3">

        <a class="btn btn-secondary mr-2"
           :class="{ disabled: submitting }"
           href="{{ $hrefCancel ?? 'javascript:history.go(-1)' }}">
          <i class="fa fa-times-circle"></i>
          Cancelar
        </a>

        <button type="submit"
                class="btn btn-primary"
                :disabled="submitting">
          <i class="fa fa-save"></i>
          Guardar
          <span x-show="submitting">@include('includes.spinner')</span>
        </button>
      </div>

