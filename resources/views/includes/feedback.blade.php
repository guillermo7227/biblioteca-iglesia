@push('scripts')
  <script>
    const feedback  = @json($feedback);

    $.toast({
      heading: feedback.titulo,
      text: feedback.texto,
      icon: feedback.tipo,
      hideAfter: 10000,
      position: 'top-center',
      showHideTransition: 'slide',
    });
  </script>
@endpush
