@extends('layouts.app')
@section('titulo', 'Prestamos')
@section('contenido')
  <div class="container">

    @auth
    {{--cerrar sesión--}}
    <div class="text-right w-100 mb-4">
      <a class="" href="{{ route('logout') }}"
         onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          <i class="fa fa-sign-out-alt fa-sm"></i>
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
    </div>
    @endauth

    @include('vue_componentes.listado-prestamos')


  </div>
@endsection



