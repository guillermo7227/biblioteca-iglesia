<!-- Este componente hace un select de cualquier entidad -->
<!-- con dos botones para 'crear nuevo/a' y 'actualizar select' -->
<label>{{ $etiqueta }}:</label>
<div class="input-group mb-3">
  <select id="{{ $campo }}"
          class="selectpicker custom-select form-control"
          data-live-search="true"
          data-live-search-normalize="true"
          title="Seleccione una opción"
          required
          name="{{ $campo }}">
    @foreach($entidades as $entidad)
      <option value="{{ $entidad->id }}"
              {{ $entidad->$campoParaDeshabilitar ? 'disabled' : '' }}>
        @if (isset($campoParaDeshabilitar) && $entidad->$campoParaDeshabilitar)
          [N/D]
        @endif
        {{ $entidad->$campoAMostrar }}
        @if (isset($campoAMostrarSegundario))
          @if($campoAMostrarSegundario == 'autores')
            (@foreach ($entidad->autores as $autor)
              {{ $autor->nombre }}
              @if (!$loop->last)
                -
              @endif
            @endforeach)
          @else
            ({{ $entidad->$campoAMostrarSegundario }})
          @endif
        @endif
      </option>
    @endforeach
  </select>

  <div class="input-group-append">
    <a class="btn btn-outline-primary"
       href="{{ route("admin.${ruta}.create") }}"
       title="Crear nuevo/a {{ str_replace('_id', '', $campo) }}">
      <i class="fa fa-plus"></i>
    </a>
  </div>

  @if ($errors->has($campo))
    <div class="alert alert-danger">
      {{ $errors->first($campo) }}
    </div>
  @endif

</div>

