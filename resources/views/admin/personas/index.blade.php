@extends('admin.layout')
@section('titulo', 'Administrar Personas')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Listado de Personas</h1>

    <div class="text-center">
      <a class="btn btn-sm btn-primary"
         href="{{ route('admin.personas.create') }}">
        <i class="fa fa-plus-circle"></i>
        Agregar una nueva Persona
      </a>
    </div>

    @foreach($personas as $persona)
      <div class="libro my-3 border rounded p-1">

        <h4>{{ $persona->nombre }}</h4>

        <div class="d-flex justify-content-between">
          <div>
            <span class="text-muted">Teléfono:</span>
            {{ $persona->telefono }} <br>

            <span class="text-muted">Email:</span>
            {{ $persona->email }}
          </div>

          <div class="ml-1 d-flex align-items-end" style="white-space: nowrap;">
            <a href="{{ route('admin.personas.show', $persona->id) }}"
               title="Ver este elemento"
               class="text-success mr-2">
              <i class="fa fa-eye"></i>
            </a>

            <a href="{{ route('admin.personas.edit', $persona->id) }}"
               title="Editar este elemento"
               class="text-info">
              <i class="fa fa-edit"></i>
            </a>
          </div>
        </div>

      </div>
    @endforeach

    <div class="d-flex justify-content-center my-5">
      {{ $personas->links() }}
    </div>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-personas').addClass('active');
  </script>
@endpush
