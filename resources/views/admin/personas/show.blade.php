@extends('admin.layout')
@section('titulo', 'Ver Persona')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Ver Persona</h1>

    <div class="d-flex justify-content-center">
      <form action="{{ route('admin.personas.destroy', $persona->id) }}"
            method="POST"
            class="mr-3">
        @method('delete')
        @csrf

        <a class="text-danger"
           title="Eliminar este elemento"
           onclick="eliminar(event)"
           href="#">
           <i class="fa fa-trash"></i>
        </a>
      </form>

      <a class="text-info"
         title="Editar este elemento"
         href="{{ route('admin.personas.edit', $persona->id) }}">
        <i class="fa fa-edit"></i>
      </a>
    </div>

    <hr>

    <span class="text-muted">Nombre:</span>
    <h4>{{ $persona->nombre }}</h4>

    <span class="text-muted">Teléfono:</span>
    <h4>{{ $persona->telefono }}</h4>

    <span class="text-muted">Email:</span>
    <h4>{{ $persona->email }}</h4>

    <div class="d-flex justify-content-center my-5">
      <a class="btn btn-sm btn-secondary"
         href="{{ route('admin.personas.index') }}">
        Ir al índice de personas
      </a>
    </div>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-personas').addClass('active');

    function eliminar(ev) {
      ev.preventDefault();

      bootbox.confirm('¿Desea eliminar este elemento?', (confirma) => {
        if (!confirma) return;

        ev.target.closest('form').submit();
      });
    }
  </script>
@endpush
