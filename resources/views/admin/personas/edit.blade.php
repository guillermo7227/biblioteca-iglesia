@extends('admin.layout')
@section('titulo', 'Editar Persona')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Editar Persona</h1>

    <form action="{{ route('admin.personas.update', $persona->id) }}"
          method="POST"
          x-data="{ submitting: false }"
          @submit="submitting = true">

      @csrf
      @method('put')

      <div class="form-group">
        <label>Nombre:</label>

        <input class="form-control"
               type="text"
               name="nombre"
               placeholder="Nombre de la persona"
               value="{{ old('nombre') ?? $persona->nombre}}"
               required>

        @if ($errors->has('nombre'))
          <div class="alert alert-danger">
            {{ $errors->first('nombre') }}
          </div>
        @endif
      </div>

      <div class="form-group">
        <label>Teléfono:</label>

        <input class="form-control"
               type="text"
               name="telefono"
               placeholder="Número de teléfono"
               value="{{ old('telefono') ?? $persona->telefono}}"
               required>

        @if ($errors->has('telefono'))
          <div class="alert alert-danger">
            {{ $errors->first('telefono') }}
          </div>
        @endif
      </div>


      <div class="form-group">
        <label>Email:</label>

        <input class="form-control"
               type="email"
               name="email"
               placeholder="Email"
               value="{{ old('email') ?? $persona->email}}">

        @if ($errors->has('email'))
          <div class="alert alert-danger">
            {{ $errors->first('email') }}
          </div>
        @endif
      </div>



      @include('includes.botones-submit')

    </form>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-personas').addClass('active');
  </script>
@endpush
