@extends('admin.layout')
@section('titulo', 'Crear nueva Persona')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Crear nueva Persona</h1>

    <form action="{{ route('admin.personas.store') }}"
          method="POST"
          x-data="{ submitting: false }"
          @submit="submitting = true">

      @csrf

      <div class="form-group">
        <label>Nombre:</label>

        <input class="form-control"
               type="text"
               name="nombre"
               value="{{ old('nombre') ?? '' }}"
               placeholder="Nombre de la persona"
               required>

        @if ($errors->has('nombre'))
          <div class="alert alert-danger">
            {{ $errors->first('nombre') }}
          </div>
        @endif
      </div>

      <div class="form-group">
        <label>Teléfono:</label>

        <input class="form-control"
               type="text"
               name="telefono"
               placeholder="Número de teléfono"
               required>

        @if ($errors->has('telefono'))
          <div class="alert alert-danger">
            {{ $errors->first('telefono') }}
          </div>
        @endif
      </div>


      <div class="form-group">
        <label>Email:</label>

        <input class="form-control"
               type="email"
               name="email"
               placeholder="Email">

        @if ($errors->has('email'))
          <div class="alert alert-danger">
            {{ $errors->first('email') }}
          </div>
        @endif
      </div>


      @include('includes.botones-submit')

    </form>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-personas').addClass('active');
  </script>
@endpush
