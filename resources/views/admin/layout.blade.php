@extends('layouts.app')
@section('contenido')
  <div class="container">

    @auth
    {{--cerrar sesión--}}
    <div class="text-right w-100">
      <a class="" href="{{ route('logout') }}"
         onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          <i class="fa fa-sign-out-alt fa-sm"></i>
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
    </div>

    <ul class="nav nav-tabs justify-content-center">
      <li class="nav-item">
        <a class="nav-link nav-link-escritorio" href="{{ route('admin.index') }}">
          <i class="fa fa-tachometer-alt"></i>
          Escritorio
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link nav-link-libros" href="{{ route('admin.libros.index') }}">
          <i class="fa fa-book"></i>
          Libros
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link nav-link-personas" href="{{ route('admin.personas.index') }}">
          <i class="fa fa-user"></i>
          Personas
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link nav-link-autores" href="{{ route('admin.autores.index') }}">
          <i class="fa fa-user-tie"></i>
          Autores
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link nav-link-cola" href="{{ route('admin.cola.index') }}">
          <i class="fa fa-tasks"></i>
          Cola
          @if ($LibroDisponibleConCola)
            <span class="text-success"><b>*</b></span>
          @endif
        </a>
      </li>
    </ul>
    @endauth

    @yield('contenido_admin')

  </div>
@endsection
