@extends('admin.layout')
@section('titulo', 'Crear nuevo Libro')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Crear nuevo Libro</h1>

    <form action="{{ route('admin.libros.store') }}"
          method="POST"
          enctype="multipart/form-data"
          x-data="{ submitting: false, descripcion: quill.root.innerHTML }"
          @submit="submitting = true">

      @csrf

      <div class="form-group">
        <label>Nombre:</label>

        <input class="form-control"
               type="text"
               name="nombre"
               placeholder="Nombre del libro"
               value="{{ old('nombre') }}"
               required>

        @if ($errors->has('nombre'))
          <div class="alert alert-danger">
            {{ $errors->first('nombre') }}
          </div>
        @endif
      </div>

      <div class="form-group">
        <label>Subtítulo:</label>

        <input class="form-control"
               type="text"
               name="subtitulo"
               placeholder="Subtítulo del libro"
               value="{{ old('subtitulo') }}">

        @if ($errors->has('subtitulo'))
          <div class="alert alert-danger">
            {{ $errors->first('subtitulo') }}
          </div>
        @endif
      </div>

      <div class="form-group">
        <label>Descripción:</label>

        @if ($errors->has('descripcion'))
          <div class="alert alert-danger">
            {{ $errors->first('descripcion') }}
          </div>
        @endif
        <div class="editor" style="height: 275px;">
          {!! old('descripcion') !!}
        </div>
        <input type="hidden" name="descripcion" :value="descripcion">
      </div>

      <fieldset class="form-group">
        <legend>Autor/es:</legend>
        <div class="form-group">
          @if ($errors->has('autores'))
            <div class="alert alert-danger">
              {{ $errors->first('autores') }}
            </div>
          @endif

          @include('vue_componentes.agregar-autor')

        </div>
      </fieldset>

      <div class="form-group">
        <label>Cantidad en inventario:</label>

        <input class="form-control"
               type="number"
               name="cantidad"
               min="0"
               value="{{ old('cantidad') ?? '1' }}">

        @if ($errors->has('cantidad'))
          <div class="alert alert-danger">
            {{ $errors->first('cantidad') }}
          </div>
        @endif
      </div>

      <div class="form-group">
        <label>Imagen de portada:</label>
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="imagen" id="imagen" accept="image/png, image/jpeg">
          <label class="custom-file-label" for="imagen">Seleccionar imagen</label>
        </div>
        @if ($errors->has('imagen'))
          <div class="alert alert-danger">
            {{ $errors->first('imagen') }}
          </div>
        @endif
      </div>

      @include('includes.botones-submit')

    </form>
  </div>
@endsection
@push('scripts')
  @include('includes.in-create-edit-libros-scripts')
  <script>
    $('.nav-link-libros').addClass('active');
    $(document).on('change', '.custom-file-input', function (event) {
        $(this).next('.custom-file-label').html(event.target.files[0].name);
    })
  </script>
@endpush
@push('styles')
  @include('includes.in-create-edit-libros-styles')
@endpush

