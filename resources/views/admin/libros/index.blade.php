@extends('admin.layout')
@section('titulo', 'Administrar Libros')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Listado de Libros</h1>

    @auth
    <div class="text-center">
      <a class="btn btn-sm btn-primary"
         href="{{ route('admin.libros.create') }}">
        <i class="fa fa-plus-circle"></i>
        Agregar un nuevo Libro
      </a>
    </div>
    @endauth

    <div class="d-flex justify-content-center my-3">
      {{ $libros->links() }}
    </div>

    <!-- buscar -->
    <div class="input-group mb-3" x-data='{criterio: "", url: @json(route('admin.libros.index'))}'>
      <input type="text"
             class="form-control"
             x-ref="inputCriterio"
             placeholder="Buscar libro o autor..."
             x-model="criterio"
             @keydown.enter="$refs.botonBuscar.click()">

      <button class="btn bg-transparent text-muted"
              title="Limpiar la búsqueda"
              style="margin-left: -40px; z-index: 100;"
              @click="criterio='';$refs.inputCriterio.focus()">
        <i class="fa fa-sm fa-times"></i>
      </button>

      <div class="input-group-append">
        <a :href="`${url}?buscar=${criterio}`"
          class="btn btn-outline-primary"
          x-ref="botonBuscar">
          <i class="fa fa-search"></i>
        </a>
      </div>
    </div>

    <div class="text-center">
      @if ($resultadosFiltrados)
        <p>Mostrando resultados para la búsqueda <code>"{{ $buscar }}"</code></p>
        <a href="{{ route('admin.libros.index') }}">Mostrar todos los libros</a>
      @endif
    </div>

    @foreach($libros as $libro)
      @continue(!$libro->hayEnInventario && \Auth::guest())
      @php
        $ruta = route('admin.libros.show', $libro->id)
      @endphp
      <div class="libro my-3 border rounded p-1">

        <div class="row">
          <div class="col-md-6 mx-auto text-center">
            <a href="{{ $ruta }}">
              <img class="img-fluid" src="{{ asset($libro->imagen) }}" alt="{{ $libro->nombre }}">
            </a>
          </div>

          <div class="col-md-6 d-flex flex-column mt-2">
            <a href="{{ $ruta }}" class="text-reset">
              <h4>{{ $libro->nombre }}</h4>
              <h5>{{ $libro->subtitulo }}</h5>
            </a>

            <div class="d-flex justify-content-between">
              <div>
                <span class="text-muted">Autor/es:</span>

                @foreach($libro->autores as $autor)
                  {{ $autor->nombre }}
                  @if(!$loop->last)
                    -
                  @endif
                @endforeach
              </div>

              <!-- acciones -->
              @auth
              <div class="" style="white-space: nowrap;">
                <a href="{{ route('admin.libros.show', $libro->id) }}"
                   title="Ver este elemento"
                   class="text-success">
                   <i class="fa fa-eye"></i>
                </a>

                <a href="{{ route('admin.libros.edit', $libro->id) }}"
                   title="Editar este elemento"
                   class="text-info">
                   <i class="fa fa-edit"></i>
                </a>
              </div>
              @endauth

            </div>

            @include('includes.disponibilidad-libro')

          </div>

        </div>
      </div>
    @endforeach

    <div class="d-flex justify-content-center my-5">
      {{ $libros->links() }}
    </div>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-libros').addClass('active');
    $(function () {
      $('#goTop').goTop({marginY:8});
    });
  </script>
@endpush
