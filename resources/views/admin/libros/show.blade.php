@extends('admin.layout')
@section('titulo', 'Ver Libro')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Ver Libro</h1>

    @auth
    <div class="d-flex justify-content-center">
      <form action="{{ route('admin.libros.destroy', $libro->id) }}"
            method="POST"
            class="mr-3">
        @method('delete')
        @csrf

        <a class="text-danger"
           title="Eliminar este elemento"
           onclick="eliminar(event)"
           href="#">
           <i class="fa fa-trash"></i>
        </a>
      </form>

      <a class="text-info"
         title="Editar este elemento"
         href="{{ route('admin.libros.edit', $libro->id) }}">
        <i class="fa fa-edit"></i>
      </a>
    </div>
    @endauth

    <hr>

    <div class="libro my-3 p-1">

      <div class="row">
        <div class="col-md-6 mx-auto">
          <img class="img-fluid" src="{{ asset($libro->imagen) }}" alt="{{ $libro->nombre }}">
        </div>

        <div class="col-md-6 d-flex flex-column">

          <span class="text-muted mt-3">Titulo:</span>
          <h4>{{ $libro->nombre }}</h4>

          @if (!is_null($libro->subtitulo) && $libro->subtitulo !== "")
            <span class="text-muted mt-3">Subtítulo:</span>
            <span>{{ $libro->subtitulo }}</span>
          @endif

          <span class="text-muted mt-3">Autor/es:</span>
          <ul class="list-unstyled">
            @foreach($libro->autores as $autor)
              <li>{{ $autor->nombre }}</li>
            @endforeach
          </ul>

          @if (!is_null($libro->descripcion) && $libro->descripcion !== "")
            <span class="text-muted mt-3">Descripción:</span>
            <div>{!! $libro->descripcion !!}</div>
          @endif

          @auth
          <div class="row mt-3">
            <div class="col">
              <span class="text-muted">Cantidad en inventario:</span>
              <span>{{ $libro->cantidad }}</span>
            </div>
            <div class="col">
              <span class="text-muted">Cantidad en préstamo:</span>
              <span>{{ $libro->cantidadEnPrestamo }}</span>
            </div>
          </div>
          @endauth

          @include('includes.disponibilidad-libro')

          @if ($libro->todosPrestados)
            <div class="text-center w-100 text-muted mt-2">
              <small>
                <a href="javascript:void(0)" onclick="apartarLibro()" class="btn btn-sm btn-outline-primary">
                    Apartar este libro cuando esté disponible
                </a>
             </small>
            </div>
          @endif

          @if ($libro->hayPersonasEnCola)
            <div class="text-center w-100 text-muted mt-2">
              <small>
                @auth()
                    <a href="{{ route('admin.cola.index') }}">
                        <i>(Actualmente hay <span id="cuenta-cola">{{ count($libro->cola) }}</span>
                          persona{{ count($libro->cola) > 1 ? 's' : ''  }} en cola)</i>
                    </a>
                @endauth
                @guest
                    <i>(Actualmente hay <span id="cuenta-cola">{{ count($libro->cola) }}</span>
                      persona{{ count($libro->cola) > 1 ? 's' : ''  }} en cola)</i>
                @endguest
             </small>
            </div>
          @endif

        </div>

      </div>
    </div>


    <div class="d-flex justify-content-center my-5">
      <a class="btn btn-sm btn-secondary"
         href="{{ route('admin.libros.index') }}">
        Ir al índice de libros
      </a>
    </div>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-libros').addClass('active');

    function eliminar(ev) {
      ev.preventDefault();

      bootbox.confirm('¿Desea eliminar este elemento?', (confirma) => {
        if (!confirma) return;

        ev.target.closest('form').submit();
      });
    }

    function apartarLibro() {
      const mensaje = `<div>
        <p style="line-height: 1.3;" class="mb-2">
          <span>Seleccione su nombre.</span> <br/>
          <small class="text-muted">
            Si no está en la lista, debe registrarse con el administrador de la biblioteca
          </small>
        </p>
      </div>`;

      const inputOptions = @json($personas).map(persona => {
        return new Object({ text: persona.nombre, value: persona.id })
      });

      bootbox.prompt({
        title: 'Ponerse en cola para préstamo',
        message: mensaje,
        inputType: 'select',
        inputOptions,
        required: true,
        onEscape: true,
        backdrop: true,
        centerVertical: true,
        callback: respuesta => {
          if (!respuesta) return;
          $.ajax({
            url: @json(route('admin.cola.store')),
            type: 'post',
            data: {
              persona_id: respuesta,
              libro_id: @json($libro->id)
            },
            success: resp => {
              $('#cuenta-cola').text(parseInt($('#cuenta-cola').text()) + 1);

              $.toast({
                text: resp.mensaje,
                position: 'top-center',
                hideAfter: 10000,
                icon: 'success',
                showHideTransition: 'slide',
              });
            },
            error: error => {
              console.log(error);
              let textoError = `[${error.status} - ${error.status == 500
                                                    ? error.responseJSON.message
                                                    : error.error.statusText}]`;
              bootbox.alert(`Error al contactar al servidor.<br>${textoError}`);
            }
          })
        }
      })
    }
  </script>
@endpush
