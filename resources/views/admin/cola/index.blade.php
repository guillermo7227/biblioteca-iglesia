@extends('admin.layout')
@section('titulo', 'Cola de Préstamos')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Cola de Préstamos</h1>

    @foreach($registros as $registro)
      <div class="libro my-3 border rounded p-1">

        @if ($registro->libro->estaDisponible)
          <span class="badge badge-pill badge-success float-right">Libro Disponible</span>
        @endif

        <div class="d-flex justify-content-between">
          <div>
            <p>
              <span class="text-muted">Libro:</span>
              <br>
              <div class="d-flex align-items-center">
                  <a href="{{ route('admin.libros.show', $registro->libro->id) }}" class="text-reset mr-1">
                    <img src="{{ $registro->libro->imagen }}" width="40">
                  </a>
                  <a href="{{ route('admin.libros.show', $registro->libro->id) }}" class="text-reset">
                    <h5>
                      {{ $registro->libro->nombre }} <br/>
                      <span>
                        <small>
                          (@foreach($registro->libro->autores as $autor)
                            {{ $autor->nombre }}
                            @if(!$loop->last)
                              -
                            @endif
                          @endforeach)
                        </small>
                      </span>
                    </h5>
                  </a>
                </div>
              </a>
            </p>

            <p>
              <span class="text-muted">Personas en cola:</span>
              <ol>
                @foreach ($registro->cola as $col)
                  <li>
                    <div class="d-flex">
                      <form action="{{ route('admin.cola.delete', [
                        $registro->libro->id,
                        $col->persona->id
                      ]) }}" method="POST">
                        @csrf
                        @method('delete')
                        <i class="fa fa-xs fa-trash text-danger mr-2"
                           style="cursor: pointer"
                           onclick="borrarPersona(event)">
                        </i>
                      </form>
                      <a href="{{ route('admin.personas.show', $col->persona->id) }}" class="text-reset">
                        {{ $col->persona->nombre }}
                      </a>
                    </div>
                  </li>
                @endforeach
              </ol>
            </p>
          </div>
        </div>

        <form action="{{ route('admin.cola.limpiar', $registro->libro->id ) }}" method="POST">
          @csrf
          @method('delete')
          <button class="btn btn-sm btn-outline-danger" onclick="limpiarCola(event)">
            <i class="fa fa-xs fa-trash"></i>
            Limpiar la cola de este libro
          </button>
        </form>

      </div>
    @endforeach

  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-cola').addClass('active');
    function borrarPersona(ev) {
      ev.preventDefault()
      bootbox.confirm('¿Borrar esta persona de la cola?', confirma => {
        if (!confirma) return;
        $(ev.target).parent('form').submit();
      })
    }
    function limpiarCola(ev) {
      ev.preventDefault()
      bootbox.confirm('¿Limpiar la cola de este libro?', confirma => {
        if (!confirma) return;
        $(ev.target).parent('form').submit();
      })
    }
  </script>
@endpush
