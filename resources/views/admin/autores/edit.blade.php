@extends('admin.layout')
@section('titulo', 'Editar Autor')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Editar Autor</h1>

    <form action="{{ route('admin.autores.update', $autor->id) }}"
          method="POST"
          x-data="{ submitting: false }"
          @submit="submitting = true">

      @csrf
      @method('put')

      <div class="form-group">
        <label>Nombre:</label>

        <input class="form-control"
               type="text"
               name="nombre"
               placeholder="Nombre del autor"
               value="{{ old('nombre') ?? $autor->nombre}}"
               required>

        @if ($errors->has('nombre'))
          <div class="alert alert-danger">
            {{ $errors->first('nombre') }}
          </div>
        @endif

      </div>


      @include('includes.botones-submit')

    </form>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-autores').addClass('active');
  </script>
@endpush
