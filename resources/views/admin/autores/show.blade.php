@extends('admin.layout')
@section('titulo', 'Ver Autor')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Ver Autor</h1>

    <div class="d-flex justify-content-center">
      <form action="{{ route('admin.autores.destroy', $autor->id) }}"
            method="POST"
            class="mr-3">
        @method('delete')
        @csrf

        <a class="text-danger"
           title="Eliminar este elemento"
           onclick="eliminar(event)"
           href="#">
           <i class="fa fa-trash"></i>
        </a>
      </form>

      <a class="text-info"
         title="Editar este elemento"
         href="{{ route('admin.autores.edit', $autor->id) }}">
        <i class="fa fa-edit"></i>
      </a>
    </div>

    <hr>

    <span class="text-muted">Nombre:</span>
    <h4 class="my-0">{{ $autor->nombre }}</h4>

    <div class="d-flex justify-content-center my-5">
      <a class="btn btn-sm btn-secondary"
         href="{{ route('admin.autores.index') }}">
        Ir al índice de autores
      </a>
    </div>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-autores').addClass('active');

    function eliminar(ev) {
      ev.preventDefault();

      bootbox.confirm('¿Desea eliminar este elemento?', (confirma) => {
        if (!confirma) return;

        ev.target.closest('form').submit();
      });
    }
  </script>
@endpush
