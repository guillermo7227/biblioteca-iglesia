@extends('admin.layout')
@section('titulo', 'Crear nuevo Autor')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Crear nuevo Autor</h1>

    <form action="{{ route('admin.autores.store') }}"
          method="POST"
          x-data="{ submitting: false }"
          @submit="submitting = true">

      @csrf

      <div class="form-group">
        <label>Nombre:</label>

        <input class="form-control"
               type="text"
               name="nombre"
               value="{{ old('nombre') ?? '' }}"
               placeholder="Nombre del autor"
               required>

        @if ($errors->has('nombre'))
          <div class="alert alert-danger">
            {{ $errors->first('nombre') }}
          </div>
        @endif
      </div>


      @include('includes.botones-submit')

      <div class="d-flex justify-content-center mt-2">

        <button type="submit"
                class="btn btn-primary mr-2 d-block"
                :disabled="submitting"
                @click="$refs.aCrearLibro.removeAttribute('disabled')">
          <i class="fa fa-save"></i>
          Guardar e ir a "Crear Libro"
          <span x-show="submitting">@include('includes.spinner')</span>
        </button>

        <!-- este input indica si redirige a "Crear nuevo Libro" -->
        <input type="hidden" name="crear-para-libro" value="true" x-ref="aCrearLibro" disabled>

      </div>

    </form>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-autores').addClass('active');
  </script>
@endpush
