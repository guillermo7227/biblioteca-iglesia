@extends('admin.layout')
@section('titulo', 'Administrar Autores')
@section('contenido_admin')
  <div class="container my-3">

    <h1 class="text-center">Listado de Autores</h1>

    <div class="text-center">
      <a class="btn btn-sm btn-primary" href="{{ route('admin.autores.create') }}">
        <i class="fa fa-plus-circle"></i>
        Agregar un nuevo Autor
      </a>
    </div>

    @foreach($autores as $autor)
      <div class="my-3 border rounded p-1">


        <div class="d-flex justify-content-between align-items-center">
          <h4 class="my-0">{{ $autor->nombre }}</h4>

          <div class="ml-1" style="white-space: nowrap;">
            <a href="{{ route('admin.autores.show', $autor->id) }}"
               title="Ver este elemento"
               class="text-success mr-2">
              <i class="fa fa-eye"></i>
            </a>

            <a href="{{ route('admin.autores.edit', $autor->id) }}"
               title="Editar este elemento"
               class="text-info">
              <i class="fa fa-edit"></i>
            </a>
          </div>
        </div>

      </div>
    @endforeach


    <div class="d-flex justify-content-center my-5">
      {{ $autores->links() }}
    </div>
  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-autores').addClass('active');
  </script>
@endpush
