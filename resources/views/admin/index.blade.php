@extends('admin.layout')
@section('titulo', 'Administración')
@section('contenido_admin')
  <div class="container my-5">

    <div class="d-flex justify-content-around flex-wrap">

        <a href="{{ route('admin.libros.create') }}"
           class="text-decoration-none my-2 mx-1">
          <div class="atajo text-dark">
            <i class="fa fa-book"></i>
            <span>Crear Libro</span>
          </div>
        </a>

        <a href="{{ route('admin.personas.create') }}"
           class="text-decoration-none my-2 mx-1">
          <div class="atajo text-dark">
            <i class="fa fa-user"></i>
            <span>Crear Persona</span>
          </div>
        </a>

        <a href="{{ route('admin.autores.create') }}"
           class="text-decoration-none my-2 mx-1">
          <div class="atajo text-dark">
            <i class="fa fa-user-tie"></i>
            <span>Crear Autor</span>
          </div>
        </a>
    </div>

  </div>
@endsection
@push('scripts')
  <script>
    $('.nav-link-escritorio').addClass('active');
  </script>
@endpush
