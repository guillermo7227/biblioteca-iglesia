@extends('layouts.app')
@section('titulo', 'Crear Préstamo')
@section('contenido')
  <div class="container">

    <h1 class="text-center">Crear Nuevo Préstamo</h1>

    <hr>

    <form action="{{ route('prestamos.store') }}"
          method="POST"
          x-data="{ submitting: false }"
          @submit="submitting = true">
      @csrf

      @component('componentes.entidad-select', [
        'etiqueta' => 'Libro',
        'campo' => 'libro_id',
        'campoAMostrar' => 'nombre',
        'entidades' => $libros,
        'ruta' => 'libros',
        'campoParaDeshabilitar' => 'todosPrestados',
        'campoAMostrarSegundario' => 'autores',
      ])
      @endcomponent

      @component('componentes.entidad-select', [
        'etiqueta' => 'Persona',
        'campo' => 'persona_id',
        'campoAMostrar' => 'nombre',
        'entidades' => $personas,
        'ruta' => 'personas',
        'campoParaDeshabilitar' => 'tieneLibro',
      ])
      @endcomponent

      <div class="form-group">
        <label>Fecha de inicio</label>
        <input type="date"
               name="fecha_inicio"
               class="form-control"
               required
               value="{{ now()->format('Y-m-d') }}">
      </div>

      <div class="form-group">
        <label>Fecha de entrega</label>
        <input type="date"
               name="fecha_entrega"
               class="form-control"
               required
               value="{{ now()->addDays(15)->format('Y-m-d') }}">
      </div>

      <hr>

       @include('includes.botones-submit', ['hrefCancel' => route('sitio.index')])
    </form>

  </div>
@endsection
@push('scripts')
  <script>
    // valor de dropdown libro_id
    $('#libro_id').val(@json(old('libro_id'))
      ? @json(old('libro_id'))
      : null).change();

    // valor de dropdown persona_id
    $('#persona_id').val(@json(old('persona_id'))
      ? @json(old('persona_id'))
      : null).change();

function submit() {
  console.log('oll');
}
  </script>
@endpush
