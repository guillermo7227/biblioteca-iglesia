@extends('layouts.app')

@section('contenido')

  <div class="container d-flex align-items-center flex-column justify-content-center"
       style="height:70vh">
    <img src="{{ asset('images/offline.png') }}"
         alt="Icono Offline"
         width="100">
    <h4 class="text-center">
      Se necesita conexión a internet para realizar esta acción.
    </h4>
  </div>

@endsection
