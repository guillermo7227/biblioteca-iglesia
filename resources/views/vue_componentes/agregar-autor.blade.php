<div id="agregar-autor">

  <div v-if="autoresAgregados.length == 0">
    <p>No hay autores agregados</p>
  </div>

  <div v-else>
    <ul>
      <li v-for="(autorAgr, i) in autoresAgregados">
        @{{ autorAgr.nombre }}
        <a class="ml-2 text-danger text-decoration-none"
           @click.prevent="quitarAutor(autorAgr)"
           href="#">
          <i class="fa fa-sm fa-times"></i>
        </a>

        <input type="hidden"
               :name="`autores[${i}]`"
               :value="autorAgr.id">
      </li>
    </ul>
  </div>

  <label class="text-muted">
    <small>
      Seleccione un autor y de click en
      <i class="fa fa-arrow-alt-circle-up"></i>
    </small>
  </label>

  <div class="input-group">
    <select id="autor_id"
            class="selectpicker custom-select form-control"
            data-live-search="true"
            data-live-search-normalize="true"
            v-model="autorSeleccionadoId"
            title="Seleccione un autor">
      <option v-for="autor in autores"
              :value="autor.id">
        @{{ autor.nombre }}
      </option>
    </select>

    <div class="input-group-append">

      <button class="btn btn-outline-primary"
              title="Agregar autor a la lista"
              @click="agregarAutor"
              type="button">
        <i class="fa fa-arrow-alt-circle-up"></i>
      </button>

      <a class="btn btn-outline-primary"
         href="{{ route("admin.autores.create") }}"
         title="Crear nuevo Autor">
        <i class="fa fa-plus"></i>
      </a>

    </div>

  </div>

</div>
@include('includes.vuejs')
<script src="{{ asset('js/rutas.js') }}"></script>

@push('scripts')
<script>
  var app = new Vue({
    el: '#agregar-autor',

    data: {
      autoresAgregados: @json(old('autores')
                              ?? $errors->has('autores')
                                 ? []
                                 : $autoresAgregados
                                   ?? []),
      autores: [],
      autorSeleccionado: undefined,
      autorSeleccionadoId: 0,
    },

    methods: {
      rutas(args) {
        return window.miApp.rutas(args);
      },

      agregarAutor() {
        if (!this.autorSeleccionado) return;

        if (this.autoresAgregados.indexOf(this.autorSeleccionado) > -1) return;

        this.autoresAgregados.push(this.autorSeleccionado);
        this.autorSeleccionadoId = 0;
        $('#autor_id').selectpicker('val', '');
      },

      quitarAutor(autor) {
        const indice = this.autoresAgregados.indexOf(autor);
        this.autoresAgregados.splice(indice, 1);
      },

      fetchAutores() {
        NProgress.start();
        $.ajax({
          url: this.rutas().api.autores.todos,
          type: 'get',
          success: (response) => {
            const autores = response.autores;
            this.autores = Object.keys(autores).map((key, index) => autores[index]);
            $('.selectpicker').selectpicker('refresh');
            setTimeout(() => $('.selectpicker').selectpicker('refresh'), 1000);
          },
          error: (error) => {
            console.log(error);
            const textoError = `[${error.status} - ${error.statusText}]`;
            bootbox.alert(`Error al contactar al servidor.<br>${textoError}`);
          },
          complete: () => {
            NProgress.done();
          }
        });
      },
    },

    watch: {
      autorSeleccionadoId(nuevo) {
        this.autorSeleccionado = this.autores.find((autor) => autor.id == nuevo);
      },

      autoresAgregados: {
        immediate: true,
        handler(nuevos) {
          const estaFetchingAutores = () => {
            if (this.autores.length > 0) {
              if (nuevos.length > 0 && typeof nuevos[0] == 'string') {
                const autoresAgr = [];
                for (const autor of nuevos) {
                  autoresAgr.push(this.autores.find((a) => a.id == autor));
                }
                this.autoresAgregados = autoresAgr;
              }
            } else {
              setTimeout(() => estaFetchingAutores(), 100);
            }
          }

          estaFetchingAutores();
        }
      }
    },

    mounted() {
      this.fetchAutores();
      console.log(@json(old('autores')));
    }
  })
</script>
@endpush
