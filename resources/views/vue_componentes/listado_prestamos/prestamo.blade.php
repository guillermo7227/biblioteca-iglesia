
@push('scripts')
<script type="text/x-template" id="prestamo-template">
  <div>
    <div class="prestamo-vistazo-rapido my-3"
         :class="{'d-none': !vistazoRapido}"
         :style="`background: ${ponerColorFondo(prestamo)}`"
         v-for="prestamo in prestamos_">

      <div class="datos">
        <div>
          <p class="nombre-libro h5 my-0">
            <a :href="rutas(prestamo.libro.id).admin.libros.show"
               class="text-reset">
              @{{ prestamo.libro.nombre }}
            </a>
          </p>
          <p class="my-0 text-muted">
            @auth
            <a :href="rutas(prestamo.persona.id).admin.personas.show"
               class="text-reset">
              @{{ prestamo.persona.nombre }}
            </a>
            |
            @endauth
            @{{ prestamo.fecha_entrega | fechaLocal }}
          </p>
        </div>

        <div class="flex-fill"></div>

        <div class="dias-restantes">
          <span class="text-muted">Restan</span>
          <span class="numero-dias">
            @{{ calcularDiasRestantes(prestamo.fecha_entrega) }}
          </span>
          <span class="text-muted">días</span>
        </div>

        @auth
        <div class="acciones ml-2">

          <a href="#"
             class="text-danger"
             @click.prevent="cancelarPrestamo(prestamo)"
             style="margin: 0.2rem;"
             title="Cancelar este préstamo">
            <i class="fa fa-xs fa-trash"></i>
          </a>

          <a href="#"
             class="text-dark"
             @click.prevent="extenderPlazo(prestamo)"
             title="">
            <i class="fa fa-xs fa-clock"></i>
          </a>

          <a href="#"
             class="text-primary"
             @click.prevent="marcarEntregado(prestamo)"
             title="Marcar como Entregado">
            <i class="fa fa-xs fa-check"></i>
          </a>

        </div>
        @endauth

      </div>
    </div>

    <div class="prestamo my-3"
         :class="{'d-none': vistazoRapido}"
         :style="`background: ${ponerColorFondo(prestamo)}`"
         v-for="prestamo in prestamos_">

      <p class="h4 py-0 my-0">
        <a :href="rutas(prestamo.libro.id).admin.libros.show"
           class="text-reset">
        @{{ prestamo.libro.nombre }}
      </a>
      </p>

      <p class="text-muted py-0 my-0">
        (<span v-for="(autor, i) in prestamo.libro.autores">
          @{{ autor.nombre }}
          @{{ i < prestamo.libro.autores.length -1 ? '-' : '' }}
        </span>)
      </p>

      <div class="datos mt-2">
        <div class="info d-flex flex-column justify-content-end">
          @auth
          <span class="text-muted">Prestado a:</span>
          <p class="h5">
            <a :href="rutas(prestamo.persona.id).admin.personas.show"
               class="text-reset">
              @{{ prestamo.persona.nombre }}
            </a>
          </p>
          @endauth

          <span class="text-muted">Fechas:</span>
          <small>
            @auth
            <span class="text-muted">Inicio:</span>
            @{{ prestamo.fecha_inicio | fechaLocal }} <br>
            @endauth
            <span class="text-muted">Entrega:</span>
            @{{ prestamo.fecha_entrega | fechaLocal}}
          </small>
        </div>

        <div class="dias-restantes px-2">
          <span class="text-muted">Restan</span>
          <span class="numero-dias">
            @{{ calcularDiasRestantes(prestamo.fecha_entrega) }}
          </span>
          <span class="text-muted">días</span>
        </div>

        @auth
        <div class="acciones">

          <a class="btn btn-sm btn-danger"
             href="#"
             @click.prevent="cancelarPrestamo(prestamo)"
             title="Cancelar este préstamo">
            <i class="fa fa-trash"></i>
          </a>

          <a class="btn btn-sm btn-dark"
             href="#"
             @click.prevent="extenderPlazo(prestamo)"
             title="Extender el plazo de entrega">
            <i class="fa fa-clock"></i>
          </a>

          <a class="btn btn-sm btn-primary"
             @click.prevent="marcarEntregado(prestamo)"
             href="#"
             title="Marcar como Entregado">
            <i class="fa fa-check"></i>
          </a>

        </div>
        @endauth

      </div>
    </div>
  </div>
</script>

<script>
  Vue.component('prestamo', {
    template: '#prestamo-template',

    props: [
      'prestamos',
      'vistazoRapido',
    ],

    data() {
      return {
        prestamos_: this.prestamos,
      }
    },

    watch: {
      prestamos(nuevo) {
        this.prestamos_ = nuevo;
      }
    },

    methods: {
      rutas(args) {
        return window.miApp.rutas(args);
      },

      extenderPlazo(prestamo) {
        bootbox.prompt({
          title: 'Extender fecha de entrega',
          inputType: 'date',
          message: 'Establezca la nueva fecha de entrega:',
          required: true,
          callback: (fecha) => {
            if (!fecha) return;

            prestamo = JSON.parse(JSON.stringify(prestamo));
            prestamo.fecha_entrega = fecha;

            NProgress.start();
            $.ajax({
              url: this.rutas(prestamo.id).api.prestamos.update,
              method: 'put',
              data: {
                prestamo: prestamo,
              },
              success: (response) => {
                this.$emit('fetch-prestamos');
                $.toast({
                  text: response.mensaje,
                  position: 'top-center',
                  hideAfter: 10000,
                  icon: 'success',
                  showHideTransition: 'slide',
                });
              },
              error: (error) => {
                console.log(error);
                const textoError = `[${error.status} - ${error.statusText}]`;
                bootbox.alert(`Error al contactar al servidor.<br>${textoError}`);
              },
              complete: () => {
                NProgress.done();
              }
            });
          }
        })
      },

      cancelarPrestamo(prestamo) {
        bootbox.confirm('¿Desea cancelar este préstamo?', (confirma) => {
          if (!confirma) return;

          NProgress.start();
          $.ajax({
            url: this.rutas(prestamo.id).api.prestamos.delete,
            method: 'delete',
            success: (response) => {
              this.$emit('fetch-prestamos');
              $.toast({
                text: response.mensaje,
                position: 'top-center',
                hideAfter: 10000,
                icon: 'success',
                showHideTransition: 'slide',
              });
            },
            error: (error) => {
              console.log(error);
              const textoError = `[${error.status} - ${error.statusText}]`;
              bootbox.alert(`Error al contactar al servidor.<br>${textoError}`);
            },
            complete: () => {
              NProgress.done();
            }
          });

        })
      },

      marcarEntregado(prestamo) {
        bootbox.confirm('¿Desea marcar este préstamo como "Entregado"?', (confirma) => {
          if (!confirma) return;

          NProgress.start();
          $.ajax({
            url: this.rutas(prestamo.id).api.prestamos.marcarEntregado,
            method: 'put',
            success: (response) => {
              this.$emit('fetch-prestamos');
              $.toast({
                text: response.mensaje,
                position: 'top-center',
                hideAfter: 10000,
                icon: 'success',
                showHideTransition: 'slide',
              });
            },
            error: (error) => {
              console.log(error);
              const textoError = `[${error.status} - ${error.statusText}]`;
              bootbox.alert(`Error al contactar al servidor.<br>${textoError}`);
            },
            complete: () => {
              NProgress.done();
            }
          });

        })
      },

      ponerColorFondo(prestamo) {
        const dias = this.calcularDiasRestantes(prestamo.fecha_entrega);

        switch (true) {
          case (dias < 0):
            return '#fba1a1';
          case (dias < 6):
            return '#fbf4a1';
          default:
            return '#a7fba1';
            // return '';
        }
      },

      calcularDiasRestantes(fechaEntrega) {
        return Math.floor((new Date(fechaEntrega) - new Date(new Date().toDateString()))/(1000*60*60*24));
      },
    },

    filters: {
      fechaLocal(fecha) {
        return new Date(fecha).toLocaleDateString();
      }
    }

  })
</script>
@endpush
