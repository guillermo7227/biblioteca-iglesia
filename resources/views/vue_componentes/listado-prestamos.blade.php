<div id="listado-prestamos">

  <!-- buscar -->
  <div class="input-group mb-3">
    <input type="text"
           id="input-buscar"
           class="form-control"
           v-model="criterioBusqueda"
           placeholder="Buscar algo">

    <button class="btn bg-transparent text-muted"
            title="Limpiar la búsqueda"
            style="margin-left: -40px; z-index: 100;">
      <i class="fa fa-sm fa-times"
         @click="criterioBusqueda = ''">
      </i>
    </button>

    <div class="input-group-append">
      <!-- <button class="btn btn&#45;outline&#45;primary"> -->
      <!--   <i class="fa fa&#45;search"></i> -->
      <!-- </button> -->
    </div>
  </div>

  <!-- ordenar -->
  <div class="input-group">
    <select id="orden"
            name="orden"
            class="form-control"
            v-model="orden">
      <option value="dias">Orden: Días restantes</option>
      <option value="libro">Orden: Titulo del libro</option>
      @auth
      <option value="persona">Orden: Nombre de la persona</option>
      @endauth
    </select>

    <a class="btn btn-sm ml-2 d-flex align-items-center"
       href="#"
       title="Vistazo rápido"
       @click.prevent="vistazoRapido = !vistazoRapido">
      <i class="fa fa-eye"></i>
    </a>
  </div>

  <prestamo :prestamos="prestamosOrdenados"
            :vistazo-rapido="vistazoRapido"
            @fetch-prestamos="fetchPrestamos">
  </prestamo>

</div>
@include('includes.vuejs')

<!-- subcomponentes -->
@include('vue_componentes/listado_prestamos/prestamo')
@push('scripts')
<script>
  var app = new Vue({
    el: '#listado-prestamos',

    data: {
      prestamos: [],
      orden: 'dias',
      criterioBusqueda: '',
      vistazoRapido: null,
    },

    methods: {
      rutas(args) {
        return window.miApp.rutas(args);
      },

      fetchPrestamos() {
        NProgress.start();
        $.ajax({
          url: this.rutas().api.prestamos.pendientes,
          type: 'get',
          success: (response) => {
            const prestamos = response.prestamos;
            this.prestamos = Object.keys(prestamos).map((key, index) => prestamos[index]);
          },
          error: (error) => {
            console.log(error);
            const textoError = `[${error.status} - ${error.statusText}]`;
            bootbox.alert(`Error al contactar al servidor.<br>${textoError}`);
          },
          complete: () => {
            NProgress.done();
          }
        });
      },

      getVistazoRapido() {
        let vistazoRapidoLS = localStorage.getItem('bigl-vistazoRapido');
        if (vistazoRapidoLS === null) {
          localStorage.setItem('bigl-vistazoRapido', true);
          vistazoRapidoLS = true;
        }
        console.log(vistazoRapidoLS);
        this.vistazoRapido = (vistazoRapidoLS == 'true');
      },
    },

    computed: {
      prestamosOrdenados() {
        switch (this.orden) {
          case 'dias':
            return this.prestamosFiltrados.sort((a, b) => {

              const fHoy = new Date(new Date().toDateString());
              const afEnt = new Date(a.fecha_entrega);
              const bfEnt = new Date(b.fecha_entrega);

              const aDiasRestantes = Math.round((afEnt - fHoy)/(1000*60*60*24));
              const bDiasRestantes = Math.round((bfEnt - fHoy)/(1000*60*60*24));

              return aDiasRestantes - bDiasRestantes;
            });

          case 'persona':
            return this.prestamosFiltrados.sort((a, b) => {
              const aNombre = a.persona.nombre.toLowerCase();
              const bNombre = b.persona.nombre.toLowerCase();

              if (aNombre < bNombre) return -1;
              if (aNombre > bNombre) return 1;

              return 0;
            });

          case 'libro':
            return this.prestamosFiltrados.sort((a, b) => {
              const aNombre = a.libro.nombre.toLowerCase();
              const bNombre = b.libro.nombre.toLowerCase();

              if (aNombre < bNombre) return -1;
              if (aNombre > bNombre) return 1;

              return 0;
            });
        }
      },

      prestamosFiltrados() {
        if (!this.criterioBusqueda) return this.prestamos;

        let items = JSON.parse(JSON.stringify(this.prestamos));

        let criterio = this.criterioBusqueda.toLowerCase();
        criterio = criterio.normalize("NFD").replace(/[\u0300-\u036f]/g, "")

        let filtrados = items.filter((item) => {
          // pone en minusculas
          let nombreLibro = item.libro.nombre.toLowerCase();
          let nombrePersona = item.persona.nombre.toLowerCase();

          // quita los acentos
          nombreLibro = nombreLibro.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
          nombrePersona = nombrePersona.normalize("NFD").replace(/[\u0300-\u036f]/g, "")

          // console.log(nombreLibro,nombrePersona,criterio);
          if (nombreLibro.search(criterio) > -1) return true;
          if (nombrePersona.search(criterio) > -1) return true;

          return false;
        });

        return filtrados;
      },
    },

    watch: {
      vistazoRapido(newValue) {
        localStorage.setItem('bigl-vistazoRapido', newValue);
      }
    },

    created() {
      this.fetchPrestamos();
    },

    mounted() {
      this.getVistazoRapido();
    }
  })
</script>
@endpush
