<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @laravelPWA

    <title>{{ config('app.name', 'Laravel') }} - @yield('titulo')</title>

    <!-- Fonts -->
    <!-- <link rel="dns&#45;prefetch" href="//fonts.gstatic.com"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> -->

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fa-all.css') }}">
    @stack('styles')
</head>
<body>
  <div id='goTop'></div>
  @include('layouts.header')

  @if (session('feedback'))
    @include('includes.feedback', ['feedback' => session('feedback')])
  @endif

  <main class="pt-4 pb-5 mb-5">
      @yield('contenido')
  </main>

  @include('layouts.navegacion')


  <!-- Scripts -->
  <script src="{{ mix('js/app.js') }}"></script>
  <script src="{{ asset('js/alpine.js') }}"></script>
  <script src="{{ asset('js/jquery.gotop.min.js') }}"></script>
  @stack('scripts')
</body>
</html>
