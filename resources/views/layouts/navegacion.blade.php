<div class="navegacion fixed-bottom bg-secondary align-items-end space-between">
  <a href="javascript:history.go(-1)"
     class="btn-navegacion"
     title="Ir Atrás">
    <i class="fa fa-lg fa-arrow-circle-left"></i>
  </a>

  <a href="{{ route('admin.libros.index') }}"
     class="btn-navegacion"
     title="Ver catálogo de libros">
    <i class="fa fa-lg fa-book"></i>
  </a>

  @auth
      <a href="{{ route('sitio.prestamos') }}"
         class="btn-navegacion"
         title="Ver prestamos">
        <i class="fa fa-lg fa-book-reader"></i>
      </a>

    <a href="{{ route('admin.index') }}"
       class="btn-navegacion"
       title="Ir a Administración">
      <i class="fa fa-lg fa-tools"></i>
      @if ($LibroDisponibleConCola)
        <span class="text-success h4"><b>*</b></span>
      @endif
    </a>
  @endauth

  @auth
    <a href="{{ route('prestamos.create') }}"
       class="btn-navegacion"
       title="Nuevo Préstamo">
      <i class="fa fa-lg fa-plus"></i>
    </a>
  @endauth

  @guest
    <a href="{{ route('login') }}"
       class="btn-navegacion"
       title="Inicia sesión">
      <i class="fa fa-lg fa-sign-in-alt"></i>
    </a>
  @endguest
</div>
