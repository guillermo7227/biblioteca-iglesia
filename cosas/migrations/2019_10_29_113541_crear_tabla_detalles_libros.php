<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaDetallesLibros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_libros', function (Blueprint $table) {
            $table->bigInteger('libro_id')->unsigned();
            $table->bigInteger('autor_id')->unsigned();
            $table->timestamps();

            $table->foreign('libro_id')->references('id')->on('libros');
            $table->foreign('autor_id')->references('id')->on('autores');

            $table->primary(['libro_id', 'autor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalles_libros');
    }
}
