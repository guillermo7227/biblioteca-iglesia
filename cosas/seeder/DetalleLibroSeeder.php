<?php

use Illuminate\Database\Seeder;
use App\DetalleLibro;

class DetalleLibroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DetalleLibro::class, 50)->create();
    }
}
