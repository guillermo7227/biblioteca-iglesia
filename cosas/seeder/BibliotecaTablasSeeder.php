<?php

use Illuminate\Database\Seeder;

class BibliotecaTablasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PersonaSeeder::class);
        $this->call(AutorSeeder::class);
        $this->call(LibroSeeder::class);
        $this->call(DetalleLibroSeeder::class);
    }
}
