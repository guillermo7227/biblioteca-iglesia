<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Persona;
use Faker\Generator as Faker;

$factory->define(Persona::class, function (Faker $faker) {
    return [
        'nombre' => ucfirst($faker->firstName() .' '. $faker->lastname()),
        'direccion' => $faker->address(),
        'telefono' => $faker->phoneNumber(),
        'fecha_nacimiento' => $faker->date(),
        'bautizado' => $faker->randomElement([true, false]),
    ];
});
