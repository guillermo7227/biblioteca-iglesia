<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class DetalleLibro extends Model
{
    protected $table = 'detalles_libros';

    protected $fillable = [
        'libro_id',
        'autor_id',
    ];

    protected $primaryKey = [
        'libro_id',
        'autor_id',
    ];

    public $incrementing = false;

    /**
     * Set the keys for a save update query.
     * This is a fix for tables with composite keys
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        foreach($this->primaryKey as $pk) {
            $query = $query->where($pk, $this->attributes[$pk]);
        }
        return $query;
    }

}
