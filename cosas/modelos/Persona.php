<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';

    protected $fillable = [
        'nombre',
        'direccion',
        'telefono',
        'fecha_nacimiento',
        'bautizado',
    ];
}
