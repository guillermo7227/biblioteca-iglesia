## App para administrar la biblioteca de la iglesia Su Palabra es Verdad
 
Puede visitar el sitio en [Biblioteca Iglesia SPEV](https://biblioteca.vallenateca.com)
 
Este sitio está hecho en Laravel 6, Bootstrap 4 y Vue 2.6.

Es una Progressive Web App y puede ser instalada en teléfonos celulares.
 
Copyright 2019 - Guillermo Agudelo
([guille.agudelo@gmail.com](mailto:guille.agudelo@gmail.com))

