var staticCacheName = "bspev-v" + new Date().getTime();
var filesToCache = [
  // '/',
  '/offline',
  '/css/app.css',
  '/js/app.js',
  '/js/rutas.js',

  // iconos app
  '/images/icons/icon-72x72.png',
  '/images/icons/icon-96x96.png',
  '/images/icons/icon-128x128.png',
  '/images/icons/icon-144x144.png',
  '/images/icons/icon-152x152.png',
  '/images/icons/icon-192x192.png',
  '/images/icons/icon-384x384.png',
  '/images/icons/icon-512x512.png',
  '/favicon.ico',

  // imagenes app
  '/images/logotexto.png',
  '/images/offline.png',

  // ajax
  '/api/prestamos/pendientes',

  //fuentes app
  'https://fonts.googleapis.com/css?family=Archivo+Narrow&display=swap',
  'https://fonts.gstatic.com/s/archivonarrow/v11/tss0ApVBdCYD5Q7hcxTE1ArZ0bbwiXw.woff2',

  // vue
  '/js/vue.js',
  '/js/vue.min.js',

  // fontawesome
  '/css/fa-all.css',
  '/webfonts/fa-solid-900.woff2'
];


// Cache on install
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                console.log('Service Worker instalado.');
                return cache.addAll(filesToCache);
            })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("bspev-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});


// Serve from Cache
self.addEventListener("fetch", event => {
    const requestURL = new URL(event.request.url);

    //Handle api calls
    if (/\/api\//.test(requestURL.pathname)) {
        event.respondWith(
            fetch(event.request).then(response => {
                event.waitUntil(
                    caches.open(staticCacheName).then(cache => {
                      cache.put(event.request, response.clone());
                    })
                );
                return response.clone();
            }).catch(function() {
            return caches.match(event.request);
          })
        );
    } else {
      event.respondWith(
          caches.match(event.request)
              .then(response => {
                  return response || fetch(event.request);
              })
              .catch(() => {
                  return caches.match('offline');
              })
      );

    }
});

