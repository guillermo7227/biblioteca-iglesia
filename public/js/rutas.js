if (!window.miApp) window.miApp = {};

window.miApp.rutas = function(...args) {
  return {
    api: {
      prestamos: {
        pendientes: '/api/prestamos/pendientes',
        update: `/api/prestamos/${args[0]}`,
        delete: `/api/prestamos/${args[0]}`,
        marcarEntregado: `/api/prestamos/${args[0]}/marcar-entregado`,
      },

      autores: {
        todos: '/api/autores/todos',
      },
    },

    admin: {
      personas: {
        show: `/admin/personas/${args[0]}`,
      },
      libros: {
        show: `/admin/libros/${args[0]}`,
      },
    }
    // canciones: {
    //   show: `/canciones/${args[0]}`,
    // }
  }
}

