<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model
{
    protected $table = 'prestamos';

    protected $fillable = [
        'libro_id',
        'persona_id',
        'fecha_inicio',
        'fecha_entrega',
        'entregado',
    ];

    protected $dates = [
        'fecha_inicio',
        'fecha_entrega',
    ];

    protected $casts = [
        'fecha_inicio' => 'date',
        'fecha_entrega' => 'date',
        'entrega' => 'boolean',
    ];

    public function getFechaInicioFormateadaAttribute()
    {
        return $this->fecha_inicio->formatLocalized('%e %b %Y');
    }

    public function getFechaEntregaFormateadaAttribute()
    {
        return $this->fecha_entrega->formatLocalized('%e %b %Y');
    }

    public function getDiasRestantesAttribute()
    {
        return $this->fecha_inicio->diffInDays($this->fecha_entrega);
    }

    public function libro()
    {
        return $this->hasOne(Libro::class, 'id', 'libro_id');
    }

    public function persona()
    {
        return $this->hasOne(Persona::class, 'id', 'persona_id');
    }
}
