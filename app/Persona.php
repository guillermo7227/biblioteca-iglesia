<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';

    protected $fillable = [
        'nombre',
        'telefono',
        'email',
    ];

    public function getTieneLibroAttribute()
    {
        return count(Prestamo::where([['persona_id',$this->id], ['entregado',false]])->get()) > 0;
    }

}
