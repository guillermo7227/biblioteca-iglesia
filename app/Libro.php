<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cola;

class Libro extends Model
{
    protected $table = 'libros';

    protected $fillable = [
        'nombre',
        'subtitulo',
        'descripcion',
        'cantidad',
        'imagen',
    ];

    public function getImagenAttribute()
    {
        $imagePath = public_path().$this->getOriginal('imagen');
        return file_exists($imagePath) ? $this->getOriginal('imagen') : 'images/libros/sinimagen.jpg';
    }

    public function autores()
    {
        return $this->belongsToMany(Autor::class, 'detalles_libros', 'libro_id', 'autor_id');
    }

    public function cola()
    {
        return $this->hasMany(Cola::class);
    }

    public function getCantidadEnPrestamoAttribute()
    {
        return count(Prestamo::where([['libro_id', $this->id], ['entregado', false]])->get());
    }

    public function getEstaDisponibleAttribute()
    {
        return $this->hayEnInventario && $this->cantidadEnPrestamo < $this->cantidad;
    }

    public function getTodosPrestadosAttribute()
    {
        return $this->hayEnInventario && $this->cantidadEnPrestamo == $this->cantidad;
    }

    public function getHayEnInventarioAttribute()
    {
        return $this->cantidad > 0;
    }

    public function getHayPersonasEnColaAttribute()
    {
        return count($this->cola) > 0;
    }
}
