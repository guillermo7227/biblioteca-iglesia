<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardarLibro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'subtitulo' => 'nullable',
            'descripcion' => 'nullable',
            'cantidad' => 'nullable|integer|min:0',
            'imagen' => 'nullable|image',
            'autores' => 'required',
        ];
    }
}
