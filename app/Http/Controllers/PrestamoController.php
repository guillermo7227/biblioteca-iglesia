<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prestamo;
use App\Persona;
use App\Libro;
use App\Http\Requests\GuardarPrestamo;
use App\Cola;

class PrestamoController extends Controller
{

    public function create()
    {
        $libros = Libro::with('autores')->get();
        $personas = Persona::all();

        return view('prestamos.create', compact(
            'libros',
            'personas'
        ));
    }

    public function store(GuardarPrestamo $request)
    {
        $input = $request->validated();

        Prestamo::create($input);

        Cola::where([
            'persona_id' => $input['persona_id'],
            'libro_id' => $input['libro_id']
        ])->delete();

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se creó el préstamo exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('sitio.index', ['forceNetwork' => true])
                         ->with('feedback', $feedback);
    }

    public function apiPendientes(Request $request)
    {
        $prestamos = Prestamo::with('libro',  'libro.autores', 'persona')
                             ->where('entregado', false)
                             ->get();

        return response()->json(compact('prestamos'));
    }

    public function apiUpdate(Request $request, $id)
    {
        $input = $request->input()['prestamo'];

        $registro = Prestamo::find($id);

        $registro->libro_id = $input['libro_id'];
        $registro->persona_id = $input['persona_id'];
        $registro->fecha_inicio = $input['fecha_inicio'];
        $registro->fecha_entrega = $input['fecha_entrega'];
        $registro->entregado = $input['entregado'];

        $registro->save();

        return response()->json([
            'mensaje' => 'Se actualizó el registro exitosamente.',
            'registro' => $registro,
        ]);
    }

    public function apiDelete(Request $request, $id)
    {
        $registro = Prestamo::find($id);

        $registro->delete();

        return response()->json([
            'mensaje' => 'Se canceló el préstamo exitosamente.',
            'registro' => $registro,
        ]);
    }

    public function apiMarcarEntregado(Request $request, $id)
    {
        $registro = Prestamo::find($id);

        $registro->entregado = true;

        $registro->save();

        return response()->json([
            'mensaje' => 'Se marcó el registro exitosamente.',
            'registro' => $registro,
        ]);
    }
}
