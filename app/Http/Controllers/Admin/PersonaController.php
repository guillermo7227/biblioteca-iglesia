<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Persona;
use App\Http\Requests\GuardarPersona;

class PersonaController extends Controller
{
    public function index()
    {
        $personas = Persona::orderBy('nombre')
                           ->paginate(15);

        return view('admin.personas.index', compact('personas'));
    }

    public function create()
    {
        return view('admin.personas.create');
    }


    public function store(GuardarPersona $request)
    {
        $input = $request->validated();

        $registro = Persona::create($input);

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se creó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.personas.show', $registro->id)
                         ->with('feedback', $feedback);

    }

    public function show($id)
    {
        $persona = Persona::findOrFail($id);

        return view('admin.personas.show', compact('persona'));
    }


    public function edit($id)
    {
        $persona = Persona::findOrFail($id);

        return view('admin.personas.edit', compact('persona'));
    }

    public function update(GuardarPersona $request, $id)
    {
        $input = $request->validated();

        $registro = Persona::findOrfail($id);

        $registro->fill($input);

        $registro->save();

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se actualizó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.personas.show', $id)
                         ->with('feedback', $feedback);

    }

    public function destroy($id)
    {
        $autor = Persona::findOrFail($id);

        $autor->delete();

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se eliminó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.personas.index')
                         ->with('feedback', $feedback);
    }

}
