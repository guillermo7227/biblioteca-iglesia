<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cola;
use App\Libro;

class ColaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('store');
    }

    public function index()
    {
        $registrosBD = Cola::with('libro')->get()->sortByDesc('libro.estaDisponible');

        $registros = [];

        foreach ($registrosBD as $reg) {
            $registros[$reg->libro_id] = (object)[
                'libro' => Libro::find($reg->libro_id),
                'cola' => Libro::find($reg->libro_id)->cola
            ];
        }

        return view('admin.cola.index', compact('registros'));
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'persona_id' => 'required|exists:personas,id',
            'libro_id' => 'required|exists:libros,id'
        ]);

        try {
            Cola::create($input);
        } catch (\Exception $ex) {
            if (strpos($ex->getMessage(), 'Duplicate entry') > -1) {
                throw new \Exception('Ya se encuentra el cola para este libro');
            }
        }

        return response()->json([
            'mensaje' => 'Se agregó a la cola satisfactoriamente. El administrador de la biblioteca lo tendrá en cuenta cuando el libro esté disponible.',
        ]);

    }


    public function delete(Request $request, $idLibro, $idPersona)
    {
        // $input = $request->validate([
        //     'persona_id' => 'required|exists:personas,id',
        //     'libro_id' => 'required|exists:libros,id'
        // ]);

        Cola::where([
            'persona_id' => $idPersona,
            'libro_id' => $idLibro
        ])->delete();

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se borró la persona de la cola satisfactoriamente',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.cola.index')
                         ->with('feedback', $feedback);
    }

    public function limpiarCola(Request $request, $idLibro)
    {
        Cola::where('libro_id', $idLibro)->delete();

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se limpió la cola satisfactoriamente',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.cola.index')
                         ->with('feedback', $feedback);

    }
}
