<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libro;
use App\Persona;
use App\Http\Requests\GuardarLibro;
use App\DetalleLibro;
use Intervention\Image\Facades\Image;

class LibroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index(Request $request)
    {
        $query = Libro::with('autores');

        $buscar = $request->buscar;
        if ($buscar) {
            $query->where('nombre', 'like', "%{$buscar}%")
                  ->orWhereHas('autores', function ($query) use ($buscar) {
                        $query->where('nombre', 'like', "%{$buscar}%");
                    });
        }

        $libros = $query->orderBy('nombre')
                        ->paginate(15);

        $resultadosFiltrados = !!$buscar;

        return view('admin.libros.index', compact('libros', 'resultadosFiltrados', 'buscar'));
    }

    public function show($id)
    {
        $libro = Libro::with('autores')->findOrFail($id);

        // Manda personas si se pueden poner en cola
        $personas = [];
        if ($libro->todosPrestados) {
            $personas = Persona::orderBy('nombre')->get();
        }

        return view('admin.libros.show', compact(
            'libro',
            'personas'
        ));
    }

    public function create()
    {
        return view('admin.libros.create');
    }


    public function store(GuardarLibro $request)
    {
        // dd($request->input(), $request->file());
        $input = $request->validated();

        $autores = $input['autores'];

        $registro = Libro::make($input);

        if ($request->hasFile('imagen')) {
            $imagen = Image::make($request->imagen);
            $imagen->resize(320, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 100);

            $rutaRelativa = '/images/libros/' . $registro->id . '-' . rand() . '.jpg';

            $imagen->save(public_path($rutaRelativa));

            $registro->imagen = $rutaRelativa;
        }

        $registro->save();

        foreach ($autores as $autor) {
            $registroDetalle = DetalleLibro::create([
                'libro_id' => $registro->id,
                'autor_id' => $autor,
            ]);
        }

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se creó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.libros.show', $registro->id)
                         ->with('feedback', $feedback);
    }


    public function edit($id)
    {
        $libro = Libro::with('autores')->findOrFail($id);

        return view('admin.libros.edit', compact('libro'));
    }

    public function update(GuardarLibro $request, $id)
    {
        $input = $request->validated();

        $autores = $input['autores'];

        $registro = Libro::findOrfail($id);

        $registro->fill($input);

        if ($request->hasFile('imagen')) {
            $imagen = Image::make($request->imagen);
            $imagen->resize(320, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 100);

            // borra archivo viejo
            if (strpos($registro->imagen, 'sinimagen.jpg') === false) {
                \File::delete(public_path($registro->imagen));
            }

            $rutaRelativa = '/images/libros/' . $registro->id . '-' . rand() . '.jpg';

            $imagen->save(public_path($rutaRelativa));

            $registro->imagen = $rutaRelativa;
        }

        $registro->save();

        DetalleLibro::where('libro_id', $id)->delete();

        foreach ($autores as $autor) {
            $registroDetalle = DetalleLibro::create([
                'libro_id' => $registro->id,
                'autor_id' => $autor,
            ]);
        }

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se actualizó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.libros.show', $id)
                         ->with('feedback', $feedback);
    }

    public function destroy($id)
    {
        $autor = Libro::findOrFail($id);

        $autor->delete();

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se eliminó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.personas.index')
                         ->with('feedback', $feedback);
    }
}
