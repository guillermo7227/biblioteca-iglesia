<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Autor;
use App\Http\Requests\GuardarAutor;

class AutorController extends Controller
{
    public function index()
    {
        $autores = Autor::orderBy('nombre')
                        ->paginate(15);

        return view('admin.autores.index', compact('autores'));
    }


    public function show($id)
    {
        $autor = Autor::findOrFail($id);

        return view('admin.autores.show', compact('autor'));
    }

    public function create()
    {
        return view('admin.autores.create');
    }


    public function store(GuardarAutor $request)
    {
        $input = $request->validated();

        $registro = Autor::create($input);

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se creó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        if ($request->input('crear-para-libro')) {
            return redirect()->route('admin.libros.create')
                             ->with('feedback', $feedback);
        }

        return redirect()->route('admin.autores.show', $registro->id)
                         ->with('feedback', $feedback);

    }

    public function edit($id)
    {
        $autor = Autor::findOrFail($id);

        return view('admin.autores.edit', compact('autor'));
    }

    public function update(GuardarAutor $request, $id)
    {
        $input = $request->validated();

        $registro = Autor::findOrfail($id);

        $registro->fill($input);

        $registro->save();

        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se actualizó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.autores.show', $id)
                         ->with('feedback', $feedback);

    }

    public function destroy($id)
    {
        $autor = Autor::findOrFail($id);

        $autor->delete();


        $feedback = [
            'tipo' => 'success',
            'texto' => 'Se eliminó el registro exitosamente.',
            'titulo' => 'Éxito',
        ];

        return redirect()->route('admin.autores.index')
                         ->with('feedback', $feedback);
    }


    public function apiTodos()
    {
        $autores = Autor::all();

        return response()->json(compact('autores'));
    }
}
