<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prestamo;
use App\Helpers\Utils;

class SitioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('prestamos');
    }

    public function index(Request $request)
    {
        if (\Auth::check()) {
            return redirect()->route('sitio.prestamos');
        }
        return redirect()->route('admin.libros.index');
    }

    public function prestamos()
    {
        return view('inicio');
    }

    public function offline()
    {
        return view('modules.laravelpwa.offline');
    }

}

