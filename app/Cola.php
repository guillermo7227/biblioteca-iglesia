<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Persona;
use App\Libro;

class Cola extends Model
{
    protected $table = 'cola';

    protected $fillable = [
        'persona_id',
        'libro_id',
    ];

    protected $primaryKey = [
        'persona_id',
        'libro_id',
    ];

    public $incrementing = false;

    public function persona()
    {
        return $this->belongsTo(Persona::class);
    }

    public function libro()
    {
        return $this->belongsTo(Libro::class);
    }

    static function hayLibrosDisponibles()
    {
        foreach (self::all() as $registro) {
            if ($registro->libro->estaDisponible) return true;
        }
        return false;
    }

}
