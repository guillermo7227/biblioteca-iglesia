<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $table = 'autores';

    protected $fillable = [
        'nombre',
    ];

    public function libros()
    {
        return $this->belongsToMany(Libro::class, 'detalles_libros', 'autor_id', 'libro_id');
    }

}
