<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// SITIO
Route::group([
    'as' => 'sitio.',
    // 'middleware' => 'auth',
], function() {

    Route::get('/', 'SitioController@index')->name('index');
    Route::get('/offline', 'SitioController@offline')->name('offline');
    Route::get('/prestamos', 'SitioController@prestamos')->name('prestamos');
});

// PRESTAMOS
Route::group([
    'as' => 'prestamos.',
    'prefix' => 'prestamos',
    'middleware' => 'auth',
], function() {

    Route::get('create', 'PrestamoController@create')->name('create');
    Route::post('store', 'PrestamoController@store')->name('store');
});

// ADMIN
Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
], function() {

    Route::get('', 'AdminController@index')->name('index');

    // ADMIN LIBROS
    Route::group([
        'prefix' => 'libros',
        'as' => 'libros.',
    ], function() {

        Route::get('', 'LibroController@index')->name('index');
        Route::get('create', 'LibroController@create')->name('create');
        Route::post('', 'LibroController@store')->name('store');
        Route::get('{id}/edit', 'LibroController@edit')->name('edit');
        Route::put('{id}', 'LibroController@update')->name('update');
        Route::get('{id}', 'LibroController@show')->name('show');
        Route::delete('{id}', 'LibroController@destroy')->name('destroy');
    });

    // ADMIN AUTORES
    Route::group([
        'prefix' => 'autores',
        'as' => 'autores.',
        'middleware' => 'auth',
    ], function() {

        Route::get('', 'AutorController@index')->name('index');
        Route::get('create', 'AutorController@create')->name('create');
        Route::post('', 'AutorController@store')->name('store');
        Route::get('{id}/edit', 'AutorController@edit')->name('edit');
        Route::put('{id}', 'AutorController@update')->name('update');
        Route::get('{id}', 'AutorController@show')->name('show');
        Route::delete('{id}', 'AutorController@destroy')->name('destroy');
    });

    // ADMIN PERSONAS
    Route::group([
        'prefix' => 'personas',
        'as' => 'personas.',
        'middleware' => 'auth',
    ], function() {

        Route::get('', 'PersonaController@index')->name('index');
        Route::get('create', 'PersonaController@create')->name('create');
        Route::post('', 'PersonaController@store')->name('store');
        Route::get('{id}/edit', 'PersonaController@edit')->name('edit');
        Route::put('{id}', 'PersonaController@update')->name('update');
        Route::get('{id}', 'PersonaController@show')->name('show');
        Route::delete('{id}', 'PersonaController@destroy')->name('destroy');
    });

    // ADMIN COLA
    Route::group([
        'prefix' => 'cola',
        'as' => 'cola.',
    ], function() {

        Route::get('', 'ColaController@index')->name('index');
        Route::post('', 'ColaController@store')->name('store');
        Route::delete('{idLibro}/{idPersona}', 'ColaController@delete')->where([
                                                                           'idLibro' => '[0-9]+',
                                                                           'idPersona' => '[0-9]+',
                                                                       ])->name('delete');
        Route::delete('{idLibro}/limpiar', 'ColaController@limpiarCola')->name('limpiar');
    });
});

// API
Route::group([
    'prefix' => 'api',
], function() {

    // API PRESTAMOS
    Route::group([
        'prefix' => 'prestamos',
    ], function() {

        // Ajax
        Route::get('pendientes', 'PrestamoController@apiPendientes');
        Route::put('{id}', 'PrestamoController@apiUpdate');
        Route::delete('{id}', 'PrestamoController@apiDelete');

        Route::put('{id}/marcar-entregado', 'PrestamoController@apiMarcarEntregado');
    });

    // API AUTORES
    Route::group([
        'prefix' => 'autores',
        'namespace' => 'Admin',
    ], function() {

        // Ajax
        Route::get('todos', 'AutorController@apiTodos');

    });
});


Auth::routes();

